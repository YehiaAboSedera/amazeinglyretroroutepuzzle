from terminaltables import AsciiTable
import json
from solvers import Solver
from models import Maze
import argparse
import io


def read_input(file):
    if isinstance(file, str):
        file = open(file, mode='r')
    room_map = None
    with file as data:
        room_map = json.load(data)
    return room_map


def print_output(maze, targets, path):
    output = [('ID', 'Room', 'Object collected')]
    for node_id in path:
        room = maze.rooms.get(node_id)
        objs = targets.intersection(room.objects)
        output.append((room.id, room.name, ",".join(
            objs) if bool(objs) else None))
    table = AsciiTable(output)
    table.outer_border = False
    table.inner_column_border = False
    print(table.table)


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--file', '-f', type=argparse.FileType('r', encoding='UTF-8'), required=False)
    parser.add_argument('--startsAt', '-s', type=int, required=False)
    parser.add_argument('--collect', '-c', type=str, nargs='+', required=False)
    args = parser.parse_args()

    return (args.file, args.startsAt, args.collect)


def get_input_from_user(file, starting_room, target_objects):
    while not file:
        file = input("Enter the path of the map file:")
    while not starting_room:
        starting_room = input("Enter the id of the starting room:")
        starting_room = int(starting_room)
    while not target_objects:
        target_objects = input("Enter the target objects (CSV):")
        target_objects = target_objects.split(",")
    return (file, starting_room, target_objects)


def main():
    file, starting_room, target_objects = get_args()
    if not file or not starting_room or not target_objects:
        file, starting_room, target_objects = get_input_from_user(
            file, starting_room, target_objects)
    print("File: %s" % file)
    print("Starting Room: %s" % starting_room)
    print("Target Objects: %s" % target_objects)

    target_objects = set(target_objects)
    rooms = read_input(file)["rooms"]
    m = Maze.from_list(rooms)
    print("Maze has %s rooms" % len(m.rooms))
    solver = Solver(m, starting_room, target_objects)
    path = solver.find_path()
    print_output(m, target_objects, path)


if __name__ == '__main__':
    main()
