from collections import defaultdict


class Room(object):
    def __init__(self, id, name, north=-1, south=-1, west=-1, east=-1, objects=[]):
        self.id = id
        self.name = name
        self.north = north
        self.south = south
        self.west = west
        self.east = east
        self.objects = [obj["name"] for obj in objects]

    def __repr__(self):
        return f'''Room[ id = {self.id}, name = {self.name},
            north = {self.north}, south = {self.south}, 
            west = {self.west}, east = {self.east},
            objects = {self.objects} ]'''


class Maze(object):
    def __init__(self, rooms):
        self.rooms = dict((room.id, room) for room in rooms)
        pass

    @staticmethod
    def from_list(rooms_data):
        rooms = list()
        for room in rooms_data:
            rooms.append(Room(**room))
        return Maze(rooms)
