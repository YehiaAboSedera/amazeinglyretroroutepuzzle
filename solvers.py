from collections import defaultdict, deque


class Node(object):
    def __init__(self,  node_id, path=[], targeted=[]):
        self.id = node_id
        self.path = path
        self.targeted = targeted

    @property
    def is_targeted_empty(self):
        return not bool(self.targeted)

    def __str__(self):
        return "{} : {} + {} ".format(self.targeted, self.path, self.id)


class Solver(object):

    def __init__(self, maze, start=1, targeted=set()):
        self.maze = maze
        self.root = start
        self.targeted_objects = targeted
        self._build_map(maze)

    def _build_map(self, maze):
        self._map = dict()
        self._targeted_rooms = set()
        found_object_count = 0
        for room in maze.rooms.values():
            connected_rooms = []
            if room.north > 0:
                connected_rooms.append(room.north)
            if room.south > 0:
                connected_rooms.append(room.south)
            if room.west > 0:
                connected_rooms.append(room.west)
            if room.east > 0:
                connected_rooms.append(room.east)
            self._map.setdefault(room.id, set()).update(connected_rooms)
            intersection = self.targeted_objects.intersection(room.objects)
            found_object_count += len(intersection)
            if bool(intersection):
                self._targeted_rooms.add(room.id)
        if found_object_count < len(self.targeted_objects):
            print("Some targeted objects are not in the maze!")

    def find_path(self):
        path = None
        queue = deque()
        queue.append(Node(self.root, [], set(self._targeted_rooms)))
        while bool(queue):
            node = queue.popleft()
            if node.id in node.targeted:
                node.targeted.remove(node.id)
            if node.is_targeted_empty:
                path = node.path
                path.append(node.id)
                break
            for adj in self._map[node.id]:
                path = list(node.path) if node.path else [node.id]
                if path[-1] != node.id:
                    path.append(node.id)
                queue.append(Node(adj, path, set(node.targeted)))
                path = None
        return path
