## Building and Rnning with Docker

###Build image
```bash
docker build -t subito.it:latest .
```
###Run Unittests
```bash
docker run --rm -it -v $(pwd):/mnt -w /mnt  subito.it ./scripts/test.sh
```
###Run the app
The app will prompt for the required inputs
```bash
docker run --rm -it -v $(pwd):/mnt -w /mnt  subito.it ./scripts/run.sh
Enter the path of the map file: <path/to/json/file>
Enter the id of the starting room: <id of starting room>
Enter the target objects (CSV):<CSV list of objects to be retreived>
```
