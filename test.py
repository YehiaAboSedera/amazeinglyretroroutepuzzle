import unittest
from models import Maze
from solvers import Node, Solver


class MazeTest(unittest.TestCase):

    def test_maze_from_list(self):
        room_data = [{
            "id": 1,
            "name": "Room 1",
            "objects": [{
                "name": "Object In Room 1"
            }]
        }]
        m = Maze.from_list(room_data)
        self.assertIsInstance(m, Maze, "Wrong object type")
        self.assertEqual(len(m.rooms), 1, "Not all data is imported")
        room = m.rooms.get(1)
        self.assertEqual(room.id, 1)
        self.assertEqual(room.name, "Room 1")
        self.assertEqual(room.north, -1)
        self.assertEqual(room.south, -1)
        self.assertEqual(room.east, -1)
        self.assertEqual(room.west, -1)
        self.assertIsInstance(room.objects, list)
        self.assertEqual(len(room.objects), 1)
        self.assertEqual(room.objects[0], "Object In Room 1")

    def test_maze_from_list_empty_list(self):
        room_data = []
        m = Maze.from_list(room_data)
        self.assertIsInstance(m, Maze, "Wrong object type")
        self.assertEqual(len(m.rooms), 0, "Not all data is imported")

    def test_maze_from_list_invalid_data(self):
        room_data = [{
            "invalid_id": 1,
            "invalid_name": "Room 1",

            "objects": [{
                "name": "Object In Room 1"
            }]
        }]
        with self.assertRaises(TypeError):
            Maze.from_list(room_data)


class NodeTest(unittest.TestCase):

    def test_is_targeted_empty(self):
        n = Node(1, [], [])
        self.assertTrue(n.is_targeted_empty)

    def test_is_targeted_not_empty(self):
        n = Node(1, [], [1])
        self.assertFalse(n.is_targeted_empty)


class SolverTest(unittest.TestCase):
    def setUp(self):
        room_data = [{
            "id": 1,
            "name": "Room 1",
            "south": 2,
            "objects": [{
                "name": "Object In Room 1"
            }]
        }, {
            "id": 2,
            "name": "Room 2",
            "north": 1,
            "objects": [{
                "name": "Object In Room 2"
            }]
        }]
        self.maze = Maze.from_list(room_data)

    def test_solver_init(self):
        s = Solver(self.maze, targeted={"Object In Room 2"})
        self.assertDictEqual(s._map, {1: {2}, 2: {1}})
        self.assertSetEqual(s._targeted_rooms, {2})
        self.assertIn(2, s.find_path())

    def test_solver_find_path(self):
        s = Solver(self.maze, targeted={"Object In Room 2"})
        p = s.find_path()
        self.assertEqual(p[0], 1)
        self.assertIn(2, p)

    def test_solver_find_path_bigger_maze(self):
        room_data = [
            {
                "id": 1,
                "name": "Hallway",
                "north": 2,
                "east": 7,
                "objects": []
            },
            {
                "id": 2,
                "name": "Dining Room",
                "north": 5,
                "south": 1,
                "west": 3,
                "east": 4,
                "objects": []
            },
            {
                "id": 3,
                "name": "Kitchen",
                "east": 2,
                "objects": [
                    {
                        "name": "Knife"
                    }
                ]
            },
            {
                "id": 4,
                "name": "Sun Room",
                "west": 2,
                "north": 6,
                "south": 7,
                "objects": []
            },
            {
                "id": 5,
                "name": "Bedroom",
                "south": 2,
                "east": 6,
                "objects": [{"name": "Pillow"}]
            },
            {
                "id": 6,
                "name": "Bathroom",
                "west": 5,
                "south": 4,
                "objects": []
            },
            {
                "id": 7,
                "name": "Living room",
                "west": 1,
                "north": 4,
                "objects": [{"name": "Potted Plant"}]
            }
        ]
        maze = Maze.from_list(room_data)
        s = Solver(maze, targeted={"Potted Plant", "Pillow", "Knife"})
        p = s.find_path()
        self.assertEqual(p[0], 1)
        self.assertIn(3, p)
        self.assertIn(5, p)
        self.assertIn(7, p)


if __name__ == '__main__':
    unittest.main()
